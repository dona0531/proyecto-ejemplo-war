import spark.servlet.SparkApplication;

import static spark.Spark.get;

public class HolaMundo implements SparkApplication {
	public static void main(String[] args) {
		new HolaMundo().init();
	}

	@Override
	public void init() {
		get("/", (req, res) -> "Bienvenido al proyecto de ejemplo");
	}
}
